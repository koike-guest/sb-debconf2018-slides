# Generating the pdf

    sudo apt install texlive-latex-recommended texlive-pictures texlive-latex-extra
    pdflatex sb-slides.tex

# Real time viewer

    sudo apt install inotify-tools
    ./latex-realtime-update.sh
