% vim: set ts=2 sts=2 sw=2 expandtab:

\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{datetime}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{positioning}
\usetheme{Boadilla}
\usecolortheme{beaver}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{swirl-lightest}}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Overview}
    \tableofcontents[currentsection]
  \end{frame}
}

\title[Secure boot status report on Debian]{Secure boot implementation status\\A report from the Debian EFI team}
\author{Debian EFI team}
\institute{DebConf 2018}
\date{July 31, 2018}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
    \tiny Slide build version: \today. \input{.git/refs/heads/master}
  \end{center}
\end{frame}

\begin{frame}{Report}
  \begin{itemize}
    \item Secure boot sprint: April 5th-8th, 2018 (Fulda)
    \item We made huge progress! \textbackslash{o/}
    \item https://salsa.debian.org/snippets/109
    \item Comments/questions - please add in Gobby: gobby.debian.org/debconf18/Sessions/UEFI+Secure+Boot
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Context}

\subsection{Secure boot explained in short}

\begin{frame}{Boot sequence}
  \begin{itemize}
    \item Firmware $\rightarrow$ Boot loader $\rightarrow$ System
    \item UEFI $\rightarrow$ Grub $\rightarrow$ Linux Kernel
  \end{itemize}
\end{frame}

\begin{frame}{Secure boot goal}
  Prevent a \textbf{REMOTE} attacker from tampering with the boot sequence.
\end{frame}

\begin{frame}{How?}

  Firmware has a set of embedded certificates; \textbf{chain of trust}

  \vspace{2cm}

  \begin{tikzpicture}[->,>=stealth',auto,node distance=2.3cm,
    thick,main node/.style={draw,font=\sffamily\Large\bfseries}]

    \node[main node] (1) {Firmware};
    \node[main node] (2) [right=of 1] {Boot loader};
    \node[main node] (3) [right=of 2] {System};

    \path[every node/.style={font=\sffamily\small}]
      (1) edge[bend right] node [below] {signature verification} (2)
      (2) edge[bend right] node [below] {signature verification} (3);
  \end{tikzpicture}
\end{frame}

\begin{frame}{Only against remote attacks?}
  \begin{itemize}
    \item UEFI allows changing certificates with physical access to the machine through \textbf{boot services}
    \item "secure boot" != "trusted/measured boot"
  \end{itemize}
\end{frame}

\subsection{SB goal for Debian}

\begin{frame}{General goal}
  \begin{itemize}
    \item Boot only \textbf{signed} binaries when SB is enabled
    \item Generic infrastructure signing any whitelisted package
    \item Make installing Debian easier for users
  \end{itemize}
\end{frame}

\begin{frame}{Inconveniences}
  \begin{itemize}
    \item Mass market computers don't trust Debian certificates
      off the shelf
    \item Current process to install Debian:
    \begin{itemize}
      \item Disable secure boot; or
      \item Install Debian certificates by ourselves
    \end{itemize}
  \item Scary to newcomers \\ (Do I need to disable \textbf{Secure} B\ldots? Doesn't sound right)
  \item Inconvenient for remote deployments \\  (any place with no easy access to the physical machine)
  \end{itemize}
\end{frame}

\begin{frame}{Microsoft}
  \begin{itemize}
    \item Machines certified by Microsoft:
    \begin{itemize}
      \item Embedded MS certificate
      \item Users are allowed to install their own certificates (x86\_64)
      \item MS has a signing service that allows organizations to get their blob signed by them
    \end{itemize}
    \item Get Grub signed by MS?
  \end{itemize}
\end{frame}

\begin{frame}{Inconvenient to get Grub signed by MS}
  \begin{itemize}
    \item MS does not sign GPLv3 code
    \item Grub's code is too big
    \item Frequent bug fixes
    \item Frequent new features
    \item Frequent updates
    \item Frequent new versions
    \item Every Grub version signed by MS $\rightarrow$ not viable
    \item Workaround: \textbf{Shim}
  \end{itemize}
\end{frame}

\subsection{What is Shim}

\begin{frame}{Shim}
  \begin{itemize}
    \item Shim is a \textbf{simple} bootloader with the only goal to load the next boot loader (Grub)
    \item Small code and non frequent new versions
    \item Shim allows embedding a certificate in its code
    \begin{itemize}
      \item Shim $\rightarrow$ signed by MS
      \item Grub $\rightarrow$ signed by Debian
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Debian boot sequence for SB}

  \begin{tikzpicture}[->,>=stealth',auto,node distance=0.9cm,
    thick,main node/.style={draw,font=\sffamily\Large\bfseries}]

    \node[main node] (1) {UEFI};
    \node[main node] (2) [right=of 1] {Shim};
    \node[main node] (3) [right=of 2] {Grub};
    \node[main node] (4) [right=of 3] {Kernel};
    \node[main node] (5) [right=of 4] {Modules};

    \path[every node/.style={font=\sffamily\small}]
      (1) edge[bend right] node [below] {MS sig verif} (2)
      (2) edge[bend left] node [above] {Debian sig verif} (3)
      (3) edge[bend right] node [below] {Debian sig verif} (4)
      (4) edge[bend left] node [above] {Debian sig verif} (5);

  \end{tikzpicture}

  \vspace{2cm}

  \begin{itemize}
    \item Debian out-of-the-box:
    \begin{itemize}
      \item SB doesn't need to be disabled
      \item Less scary to users
    \end{itemize}
    \item "Assurance" that boot sequence was not tempered by a remote attack
  \end{itemize}
\end{frame}

\section{Signing Infrastructure}

\subsection{Package generation}

\begin{frame}{Package generation}
  \begin{tikzpicture}[->,>=stealth,auto,node distance=1.5cm and 2cm,
    main node/.style={draw,font=\sffamily\normalsize\bfseries}]

    \node[main node] (1) {pkg.dsc};
    \node[main node] (2) [above right=2.5cm of 1.west,anchor=west] {pkg.deb};
    \node[main node] (3) [below right=2.5cm of 1.west,anchor=west] {sig-template.deb};
    \node[main node] (4) [right=of 3] {sig.dsc};
    \node[main node] (5) [right=of 4] {sig.deb};

    \path[every node/.style={font=\sffamily\small,align=center}]
      (1) edge[bend left] node [left] {DD build} (2)
      (1) edge[bend right] node [left] {buildd} (3)
      (3) edge[right] node [above,text width=3cm] {Signing service\\generate} (4)
      (4) edge[right] node [above,text width=3cm] {Signing service\\build} (5);

  \end{tikzpicture}

  \vspace{2cm}

  *.dsc represents the source package
\end{frame}

\begin{frame}{sig.dsc: the source package of the signed version}
  \begin{itemize}
    \item Sig source package (sig.dsc) is generated \textbf{automatically} by the signing service
    \item It contains detached signatures
    \item Its build depends on the unsigned pkg.deb
    \item Build is simple: attaches signatures to the files from pkg.deb
    \item \textbf{Build is reproducible}
  \end{itemize}
\end{frame}

\begin{frame}{Package flow}
  \begin{tikzpicture}[->,>=stealth,auto,node distance=2.3cm, align=center,
    main node/.style={draw,font=\sffamily\normalsize\bfseries}]

    \node[main node] (1) {DD};
    \node[main node] (2) [right=of 1] {Dak};
    \node[main node] (3) [right=of 2,text width=2cm] {Signing\\service};
    \node[main node] (4) [right=of 3] {Dak};

    \path[every node/.style={font=\sffamily\small,align=center}]
    (1) edge[right] node [above] {dput} (2)
    (2) edge[right] node [above] {informs} (3)
    (3) edge[right] node [above] {dput} (4);

  \end{tikzpicture}

  \vspace{2cm}

  \begin{itemize}
    \item Signing service
    \begin{itemize}
      \item Polls dak for the list of packages to be signed
      \item Maintains an audit log of every file that got signed
    \end{itemize}
    \item Dak: Debian archive kit
      \begin{itemize}
      \item Verify that the package to be signed is on an approved list
      \item Exports a manifest of packages that should be signed
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Template binary package}

\begin{frame}{Template binary package structure}
  \begin{itemize}
    \item dpkg -x template.deb
    \item /usr/share/code-signing/$<$template-bin-pkg-name$>$/
    \begin{itemize}
      \item files.json:\\ \tab contains a list of files to be signed
      \item source-template/:\\ \tab folder with the structure to generated the new source package
    \end{itemize}
  \item Signing service copies detached signatures to debian/signatures/
  \item Then it executes: dpkg-genchanges … \&\& debsign ... \&\& dput ...
  \end{itemize}
\end{frame}

\section{Current status}

\subsection{Signing service}

\begin{frame}{Signing service - current status}
  \begin{itemize}
    \item Code available at https://salsa.debian.org/ftp-team/code-signing \\ (SB Sprint 2018)
    \item State: functional and deployed in experimental suite \\ signing packages with a dummy Debian key
    \item Manual process for now
    \item Audit log kept in a PostgreSQL DB with public database dumps
  \end{itemize}
\end{frame}

\begin{frame}{Signing service - TODOs}
  \begin{itemize}
    \item Improve tests
    \item Think about key and signature revocation process
    \item Deploy to stable / testing / unstable
    \item Nice to have: nice browser for audit log
  \end{itemize}
\end{frame}

\subsection{Dak}

\begin{frame}{DAK}
  \begin{itemize}
    \item Published list of packages to be signed
    \item Accept source only uploads by the signing service (including new ones)
    \item Embargoed packages: please, do not reuse version numbers when the first package is rejected
  \end{itemize}
\end{frame}

\subsection{Packages}

\begin{frame}{Packages to be signed}
  \begin{itemize}
    \item shim boot services
    \item fwupd/fwupdate
    \item grub2
    \item kernel
    \item systemd-boot(?)
  \end{itemize}
\end{frame}

\begin{frame}{shim}
  \begin{itemize}
    \item Signed by MS' key, has our root certificate embedded
    \item Currently not reproducible due to ephemereal embedded keys\\
      … but patches for this exists
    \item Currently on an old version
  \end{itemize}
\end{frame}

\begin{frame}{fwupd/fwupdate}
  \begin{itemize}
    \item Packages to enable secure firmware updates during reboot
    \item Added support for building signing template packages
    \item Recommend the -signed versions of the binary packages
    \item Install the -signed versions of binaries in the ESP in
      preference
    \item Most recent uploads to unstable contain all needed changes
  \end{itemize}
\end{frame}

\begin{frame}{grub2}
  \begin{itemize}
    \item If booting with SB enabled, will \textbf{only} boot a signed
      kernel
    \item Builds a monolithic grub image for signing
    \item Added support for building signing template packages
    \item Recommend the -signed versions of the binary packages
    \item Install the -signed versions of binaries in the ESP in
      preference
    \item Most recent upload to unstable contains all needed changes
  \end{itemize}
\end{frame}

\begin{frame}{kernel}
  \begin{itemize}
    \item Source package: support for building signing template packages
    \item Currently only enabled in uploads to experimental
    \item One upload to experimental was successfully signed
    \item Unstable will be enabled once the signing service is running on unstable
  \end{itemize}
\end{frame}

\begin{frame}{work still needed}
  \begin{itemize}
    \item CD releases
    \item live images
    \item cloud images
    \item image generation tools
  \end{itemize}
\end{frame}

\begin{frame}{Thanks}

\begin{center}
\Huge Questions?
\end{center}

\begin{center}
Creative Commons Attribution-ShareAlike 4.0
\end{center}

\end{frame}
\end{document}
