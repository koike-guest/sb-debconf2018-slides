#!/bin/bash

pdflatex sb-slides.tex
evince sb-slides.pdf &

while sleep 1; do
	inotifywait -e close_write sb-slides.tex
	pdflatex -interaction=nonstopmode sb-slides.tex
	pdflatex -interaction=nonstopmode sb-slides.tex
done
